# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import os

from celery.schedules import crontab
# from celery_once import QueueOnce
from django.apps import apps
from django.utils import timezone
# from kombu import Queue, Exchange
from celery import Celery
# from celery.schedules import crontab
# import datetime

# from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'IpamV1.settings')

app = Celery('ipam')

app.conf.beat_schedule = {
    'ipam.ip_am_update_main': {
        'task': "open_ipam.tasks.ip_am_update_main",
        'schedule': crontab(hour=2, minute=0)  # 每天凌晨两点更新total_ip_list的地址
        # 'schedule': datetime.timedelta(days=1),
        # 'schedule': 180
    }
}
app.now = timezone.now
app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks(lambda: [n.name for n in apps.get_app_configs()])
