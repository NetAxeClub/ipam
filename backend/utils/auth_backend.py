from django.contrib.auth.backends import BaseBackend

class NoAuthBackend(BaseBackend):
    def authenticate(self, request, **kwargs):
        return None  # 永远返回 None，表示不验证用户

    def get_user(self, user_id):
        return None  # 永远返回 None，表示不获取用户