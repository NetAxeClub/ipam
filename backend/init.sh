#!/bin/bash
python3 manage.py makemigrations users
python3 manage.py migrate users
#python3 manage.py makemigrations
#python3 manage.py migrate open_ipam

# 做静态文件收集
#echo -e "yes" | python3 manage.py collectstatic
python3 manage.py collectstatic --no-input
echo "init success!"
echo "starting web server!"
sh start.sh web