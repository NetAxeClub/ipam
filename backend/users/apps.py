from django.apps import AppConfig
from utils.nacos_register import nacos_init


class UsersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'users'

    def ready(self):
        nacos_init()